# ofzza's Proxy server

HTTP proxy server service supporting content modifications via plugins and implementing a web configuration interface.

Done ...
- Implemented proxy server functionality 
- Implemented dummy web configuration interface

To do ...
- Implement plugin/content editing functionality 
- Implemented full web configuration interface