﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ofzza.playground.ProxyServer {
    
    /// <summary>
    /// Gets / Sets server's persistant properties
    /// </summary>
    public class Preferences {

        // =============================================================================================================
        // Connection properties
        // =============================================================================================================
        #region Connection properties

        /// <summary>
        /// Gets / Sets incoming / listening port number
        /// </summary>
        public static int ListeningPort {
            get { return 18080; }
            set { }
        }
        /// <summary>
        /// Gets / Sets maximum length of listening backlog
        /// </summary>
        public static int ListeningBacklog {
            get { return 64; }
            set { }
        }

        #endregion

        // =============================================================================================================
        // Connection timeouts
        // =============================================================================================================
        #region Connection timeouts

        /// <summary>
        /// Gets / Sets timeout between socket reads (in [ms])
        /// </summary>
        public static int TimeoutBetweenReads {
            get { return 100; }
            set { }
        }

        /// <summary>
        /// Gets / Sets HTTP header receiving timeout (in [ms])
        /// </summary>
        public static int ReadHeaderTimeout {
            get { return 100; }
            set { }
        }

        /// <summary>
        /// Gets / Sets HTTP response timeout (in [ms])
        /// </summary>
        public static int HttpResponseTimeout {
            get { return 30000; }
            set { }
        }

        #endregion

    }

}
