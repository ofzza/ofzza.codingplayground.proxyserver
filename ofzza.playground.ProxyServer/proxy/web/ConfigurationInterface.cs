﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using ofzza.framework.windows.interop.net.http;

namespace ofzza.playground.ProxyServer.proxy.web {
    
    /// <summary>
    /// Generates and serves configuration web interface
    /// </summary>
    public class ConfigurationInterface {

        // =============================================================================================================
        // Constructors & initialization
        // =============================================================================================================
        #region Constructors & initialization

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="requestSocket">HTTP request socket reference</param>
        /// <param name="requestPath">HTTP request path</param>
        public ConfigurationInterface(Socket requestSocket, string requestPath) {
            // Set request data
            this._requestSocket = requestSocket;
            this._requestPath = requestPath;
            // Serve up interface
            this.ServeInterface();
        }

        #endregion

        // =============================================================================================================
        // Properties
        // =============================================================================================================
        #region Properties

        /// <summary>
        /// Holds reference to the incoming HTTP request socket
        /// </summary>
        protected Socket _requestSocket = null;
        /// <summary>
        /// Holds HTTP request path
        /// </summary>
        protected string _requestPath = null;

        /// <summary>
        /// Gets login page HTML
        /// </summary>
        protected byte[] LoginHtml {
            get {
                try {

                    // Get stream reader
                    Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("ofzza.playground.ProxyServer.proxy.web.html.login.html");
                    byte[] buffer = new byte[s.Length];
                    s.Read(buffer, 0, buffer.Length);
                    // Dispose stream
                    s.Close();
                    s.Dispose();
                    // Return HTML
                    return buffer;

                } catch (Exception ex) {

                    // Return empty
                    return new byte[0];

                }
            }
        }
        /// <summary>
        /// Gets configuration page HTML
        /// </summary>
        protected byte[] ConfigurationHtml {
            get {
                try {

                    // Get stream reader
                    Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("ofzza.playground.ProxyServer.proxy.web.html.configuration.html");
                    byte[] buffer = new byte[s.Length];
                    s.Read(buffer, 0, buffer.Length);
                    // Dispose stream
                    s.Close();
                    s.Dispose();
                    // Return HTML
                    return buffer;

                } catch (Exception ex) {

                    // Return empty
                    return new byte[0];

                }
            }
        }
        /// <summary>
        /// Gets requested resource pagr
        /// </summary>
        protected byte[] RequestedResource {
            get {
                try {

                    // Get stream reader
                    Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("ofzza.playground.ProxyServer.proxy.web.html." + this._requestPath.Replace('/', '.').Replace('\\', '.'));
                    byte[] buffer = new byte[s.Length];
                    s.Read(buffer, 0, buffer.Length);
                    // Dispose stream
                    s.Close();
                    s.Dispose();
                    // Return HTML
                    return buffer;

                } catch (Exception ex) {

                    // Return empty
                    return new byte[0];

                }
            }
        }

        #endregion

        // =============================================================================================================
        // Web interface functinality
        // =============================================================================================================
        #region Web interface functinality

        /// <summary>
        /// Serves up web interface
        /// </summary>
        protected void ServeInterface() {
        
            // Compose web interface
            byte[] content = this.ComposeInterface();

            // Send response header
            HttpHeader header = new HttpHeader();
            header.ComposeResponseHeader(this._requestPath, content);
            this._requestSocket.Send(header.EncodedContent);

            // Send response content
            this._requestSocket.Send(content);

        }

        /// <summary>
        /// Composes configuration web interface HTML
        /// </summary>
        /// <returns>Web interface HTML</returns>
        protected byte[] ComposeInterface() {
            // Check requested path
            if ((this._requestPath != null) && (this._requestPath.Length > 0)) {
                // Process request
                return this.RequestedResource;
            } else{
                // Generate configuration page
                return this.ConfigurationHtml;
            }
        }

        #endregion


    }

}
