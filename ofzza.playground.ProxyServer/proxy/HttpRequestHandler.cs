﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ofzza.framework.windows.interop.net.http;
using ofzza.playground.ProxyServer.proxy.web;

namespace ofzza.playground.ProxyServer.proxy {
    
    /// <summary>
    /// Handles incoming HTTP requests
    /// </summary>
    public class HttpRequestHandler {

        // =============================================================================================================
        // Constructors & initialization
        // =============================================================================================================
        #region Constructors & initialization

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="incomingSocket">Incoming request socket</param>
        public HttpRequestHandler(Socket incomingSocket) {
            // Store incoming request socket
            this._requestSocket = incomingSocket;
            // Handle request in new thread
            this._thread = new Thread(this.HandleHttpRequest);
            this._thread.IsBackground = true;
            this._thread.Start();
        }

        #endregion

        // =============================================================================================================
        // Properties
        // =============================================================================================================
        #region Properties

        /// <summary>
        /// Holds reference to the incoming HTTP request socket
        /// </summary>
        protected Socket _requestSocket = null;
        /// <summary>
        /// Holds reference to internal processing thread
        /// </summary>
        protected Thread _thread = null;

        #endregion

        // =============================================================================================================
        // Request handling
        // =============================================================================================================
        #region Request handling

        /// <summary>
        /// Handles incoming HTTP request
        /// </summary>
        protected void HandleHttpRequest() {

            try {

                // Initialize HTTP header
                HttpHeader requestHeader = new HttpHeader();
                if (requestHeader.ReadHeader(this._requestSocket, Preferences.ReadHeaderTimeout)) {

                    // Prompt processing request
                    ProxyServerService.ConsoleWrite(ConsoleColor.Gray, string.Format("> Processing request for {1} from {0}", this._requestSocket.RemoteEndPoint.ToString(), requestHeader.TargetURL));

                    // Check reuqested domain

                    if ((requestHeader.TargetHost == "configuration.proxy") || (requestHeader.TargetPort == Preferences.ListeningPort)) {

                        // Serve configuration web interface
                        // ----------------------------------------------------------

                        // Serve up configuration web interface
                        ConfigurationInterface webInterface = new ConfigurationInterface(this._requestSocket, requestHeader.TargetPath);

                        // Abort processing
                        this.Abort(null);
                        return;

                    } else {

                        // Forward received HTTP request
                        // ----------------------------------------------------------

                        // Connect to remote host
                        Socket forwardSocket = new Socket(this._requestSocket.AddressFamily, this._requestSocket.SocketType, this._requestSocket.ProtocolType);
                        forwardSocket.Connect(new DnsEndPoint(requestHeader.TargetHost, requestHeader.TargetPort));

                        // Forward request
                        forwardSocket.Send(requestHeader.EncodedContent);

                        // Wait for response
                        DateTime responceTimestamp = DateTime.Now;
                        while ((!forwardSocket.Poll(0, SelectMode.SelectError)) && (forwardSocket.Available == 0) && ((DateTime.Now - responceTimestamp).TotalMilliseconds < Preferences.HttpResponseTimeout)) {
                            // Wait for response
                            int delay = (int)(DateTime.Now - responceTimestamp).TotalMilliseconds;
                            Thread.Sleep((delay < 500 ? delay : 500));
                        }
                        if (forwardSocket.Available == 0) {
                            // Abort processing
                            this.Abort(forwardSocket);
                            return;
                        }

                        // Read HTTP header from forwarded response
                        HttpHeader forwardedHeader = new HttpHeader();
                        if (forwardedHeader.ReadHeader(forwardSocket, Preferences.ReadHeaderTimeout)) {

                            // Forward response header
                            this._requestSocket.Send(forwardedHeader.EncodedContent);

                            // Process response
                            byte[] buffer = null;
                            long totalLength = 0;
                            DateTime dataReceivedTimestamp = DateTime.Now;
                            while ((!this._requestSocket.Poll(0, SelectMode.SelectError)) && ((forwardSocket.Available > 0) || (!forwardSocket.Poll(0, SelectMode.SelectError))) && ((DateTime.Now - responceTimestamp).TotalMilliseconds < Preferences.HttpResponseTimeout)) {
                                // Check for data
                                if (forwardSocket.Available > 0) {

                                    // Register new data
                                    dataReceivedTimestamp = DateTime.Now;

                                    // Process data
                                    buffer = new byte[forwardSocket.Available];
                                    totalLength += forwardSocket.Receive(buffer);
                                    this._requestSocket.Send(buffer);

                                } else {

                                    // Wait for data
                                    int delay = (int)(DateTime.Now - dataReceivedTimestamp).TotalMilliseconds;
                                    Thread.Sleep((delay < 500 ? delay : 500));

                                }
                                // Check if data over
                                if (forwardedHeader.TransmissionMode == TransmissionMode.Undefined) break;
                                if ((forwardedHeader.TransmissionMode == TransmissionMode.FixedLength) && (forwardedHeader.ContentLength <= totalLength)) break;
                                if ((forwardedHeader.TransmissionMode == TransmissionMode.Chunked) && (buffer != null) && (buffer.Length == 1) && (buffer[0] == (byte)'0')) break;
                            }


                        } else {

                            // Abort processing
                            this.Abort(forwardSocket);
                            return;

                        }

                    }

                } else {

                    // Abort processing
                    this.Abort(null);
                    return;

                }

            } catch (Exception ex) {
                
                // Prompt exception
                ProxyServerService.ConsoleWrite(ex);

            }

            // Done processing
            this.Abort(null);
            return;

        }

        /// <summary>
        /// Aborts request processing
        /// </summary>
        /// <param name="forwardSocket">Forwarding request socket</param>
        protected void Abort(Socket forwardSocket) {
            // Abort forwarded socket
            if (forwardSocket != null) {
                forwardSocket.Close();
                forwardSocket.Dispose();
            }
            // Abort request socket
            this._requestSocket.Close();
            this._requestSocket.Dispose();
        }

        #endregion

    }

}
