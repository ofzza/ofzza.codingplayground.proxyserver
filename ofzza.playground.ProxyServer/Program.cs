﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ofzza.framework.windows.application;
using ofzza.framework.windows.services;

namespace ofzza.playground.ProxyServer {

    /// <summary>
    /// Service installer information
    /// </summary>
    public class ProxyServerServiceInstaller : StandaloneServiceInstaller { }

    /// <summary>
    /// Alarm service entry point
    /// </summary>
    public class ProxyServerService : StandaloneService {

        // =============================================================================================================
        // Constructors & initialization
        // =============================================================================================================
        #region Constructors & initialization

        /// <summary>
        /// Constructor
        /// </summary>
        public ProxyServerService() {
            // Set service name
            this.ServiceName = "ofzza's Proxy Server service";
        }

        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args">Startup arguments</param>
        new static void Main(string[] args) {
            // Set application identity
            Identity.ApplicationTitle = "ofzza's Proxy Server service";
            Identity.ApplicationCompany = "ofzza";
            Identity.ApplicationProduct = "ProxyServerService";
            Identity.ApplicationPersistenceContext = PersistenceContext.LocalMashine;
            // Set installer properties
            ProxyServerServiceInstaller.ServiceName = "ofzza's Proxy Server service";
            ProxyServerServiceInstaller.ServiceDisplayName = "ofzza's Proxy Server service";
            // Prompt startup status
            ProxyServerService.StartupStatusInformation = "ofzza's Proxy Server service" + "\r\n" +
                                                          "\r\n" +
                                                          "\t > Listening as HTTP Proxy on port :18080" + "\r\n" +
                                                          "\t > Web configuration interface avaliable at locahost:18080";
            // Attach functionality
            ProxyServerService.OnServiceStart = ProxyServerService.ExtendedOnServiceStart;
            ProxyServerService.OnServiceStop = ProxyServerService.ExtendedOnServiceStop;
            // Do service initialization
            if (ProxyServerService.Initialize(args, ProxyServerService.ProcesssStartupArguments, ProxyServerService.ServiceDependantHelp)) {

                // Run service
                ProxyServerService.Run();

            }
        }

        /// <summary>
        /// Gets service dependant help information
        /// </summary>
        /// <returns>Help information</returns>
        protected new static string ServiceDependantHelp() {
            // Return help
            return "";
        }
        /// <summary>
        /// Does service's custom processing of startup arguments
        /// </summary>
        /// <param name="args">Startup arguments</param>
        /// <returns>If service functionality should start</returns>
        protected new static bool ProcesssStartupArguments(string[] args) { return false; }

        #endregion

        // =============================================================================================================
        // Functionality
        // =============================================================================================================
        #region Functionality

        /// <summary>
        /// On service start functionality
        /// </summary>
        protected static void ExtendedOnServiceStart() {
            // Prompt
            ProxyServerService.ConsoleWrite(ConsoleColor.Green, "ofzza's Proxy Server service started ...");
            // Start service
            Service.Start();

        }
        /// <summary>
        /// On service stop functionality
        /// </summary>
        protected static void ExtendedOnServiceStop() {
            // Prompt
            ProxyServerService.ConsoleWrite(ConsoleColor.Green, "ofzza's Proxy Server service stopped ...");
            // Stop service
            Service.Stop();

        }

        #endregion

    }

}
