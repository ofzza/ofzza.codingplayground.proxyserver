﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ofzza.framework.windows.interop.net.proxy;
using ofzza.playground.ProxyServer.proxy;

namespace ofzza.playground.ProxyServer {
    
    /// <summary>
    /// Implements service's start / stop functionality
    /// </summary>
    public class Service {

        // =============================================================================================================
        // Start / Stop functionality
        // =============================================================================================================
        #region Start / Stop functionality

        /// <summary>
        /// Starts service's functionality
        /// </summary>
        public static void Start() {
            // Check status
            if ((!Service._started) && ((Service._thread == null) || (!Service._thread.IsAlive))) {
                // Set status
                Service._started = true;
                // Start listening thread
                Service._thread = new Thread(Service.Listen);
                Service._thread.IsBackground = true;
                Service._thread.Start();
            }
        }

        /// <summary>
        /// Stops service's functionality
        /// </summary>
        public static void Stop() {
            // Check status
            if (Service._started) {
                // Set status
                Service._started = false;
                // Kill listening socket
                if (Service._socket != null) {
                    Service._socket.Close();
                    Service._socket.Dispose();
                    Service._socket = null;
                }
                // Kill listening thread
                if ((Service._thread != null) && (Service._thread.IsAlive)) {
                    Service._thread.Abort();
                    Service._thread = null;
                }
            }
        }

        #endregion

        // =============================================================================================================
        // Properties
        // =============================================================================================================
        #region Properties

        /// <summary>
        /// Holds started status
        /// </summary>
        protected static bool _started = false;

        /// <summary>
        /// Holds reference to main listening thread
        /// </summary>
        protected static Thread _thread = null;
        /// <summary>
        /// Holds main incoming connections socket reference
        /// </summary>
        protected static Socket _socket = null;

        #endregion

        // =============================================================================================================
        // Functionality
        // =============================================================================================================
        #region Functionality

        /// <summary>
        /// Listens for incoming connections
        /// </summary>
        protected static void Listen() {
            // Loop while started
            while (Service._started) {
                
                // Check socket
                if (Service._socket == null) {
                    // Initialize socket
                    Service._socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                    Service._socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), Preferences.ListeningPort));
                }

                // Listen for incoming connection
                Service._socket.Listen(Preferences.ListeningBacklog);
                Socket incomingSocket = Service._socket.Accept();
                
                // Process incoming request
                HttpRequestHandler requestHandler = new HttpRequestHandler(incomingSocket);

            }
        }

        #endregion

    }

}
